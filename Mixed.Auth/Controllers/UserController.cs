﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.IISIntegration;

namespace Mixed.Auth.Controllers
{
    [Route("users")]
    public class UserController : Controller
    {
        // GET api/values
        // curl -H @{"authorization" = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoicG5sXFxnYWJlIiwic3ViIjoicG5sXFxnYWJlIiwianRpIjoiYWEyMzllNzgtOTRhYy00NjhiLWIwY2ItMDYyNGNmNDhhYmEwIiwiZXhwIjoxNTU3MTUzNTg1fQ.6YShe7Qgxi0VQWfa4_3stgobb_UefIf1KyxKttGDfVA"} "http://localhost:50820/users"
        [HttpGet]
        public ActionResult<IIdentity> Get()
        {
            var identity = User.Identity;

            return Ok(new
            {
                Name = identity.Name,
                IsAuthenticated = identity.IsAuthenticated,
                AuthenticationType = identity.AuthenticationType,
            });
        }
    }
}
