# NTLM Got you down?

NTLM is a convenient, if hacky, way of providing end-user authentication
asp.net web applications. However, it can become a real pain in many situations:

* In testing and development there is no easy way to impersonate different users with different rights
* Many automated tools don't support NTLM well, or at all
* Server-to-server communication can be a hassle

This repo shows a simple way to combine the convenience of NTLM with the
benefits of a real HTTP authentication scheme, namely using [JSON Web Tokens](https://jwt.io/introduction/).

## How it works

* Authentication is set to use `Authorization: Bearer [JWT]` first.
* If there is no `Authorization` header, fall back to NTLM

Where does one get a JWT? Well, since you're the project owner you could
make a little command line tool that generates a JWT for any user, which you could then use
for testing and development while impersonating different users.

This project also contains a controller that issues a JWT based on the user's current windows identity.
In a real app you would probably copy various attributes besides the username to the claims
(such as Active Directory roles).

It also will issue a JWT for any other username you supply. This could be an easy starting point
to impersonating different users during development. In a real app you would probably restrict this
to super users, and perhaps disable for production builds.

## Take it for a test spin

* Run the project
* On a powershell command line, run `curl "http://localhost:50820/users"` and verify that you get a `(401) Unauthorized` response
* Open the [swagger](http://localhost:50820/swagger/index.html) page for the project and run the `/tokens/current` endpoint to obtain a token.
  The token will be a long-ish string of base-b4 encoded data.
* Back in powershell, run `curl -H @{"authorization" = "Bearer [JWT]""} "http://localhost:50820/users"` (substituting the actual token for [JWT]).
  Verify that you now get a `200` response with basic user data as the response content.
