﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Formatters.Internal;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace Mixed.Auth
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = IISDefaults.AuthenticationScheme;
//                    options.AddScheme("", b => b.HandlerType);
                })
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateAudience = false,
                        ValidateIssuer = false,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey =
                            new SymmetricSecurityKey(Encoding.UTF8.GetBytes("74C9C7C7-0954-4330-997A-FA919100C6DE")),
                        ValidateLifetime = true, //validate the expiration and not before values in the token
                        ClockSkew = TimeSpan.FromMinutes(1), //1 minute tolerance for the expiration date
                    };
                });

            services.AddSwaggerGen(options =>
            {
                var section = Configuration.GetSection("Swagger");

                if (section["Version"] != null && section["Title"] != null)
                {
                    options.SwaggerDoc(
                        section["Version"],
                        new Info { Title = section["Title"], Version = section["Version"] }
                    );
                }

                if (section["XmlComments"] != null)
                {
                    var path = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, section["XmlComments"]);

                    if (File.Exists(path))
                    {
                        options.IncludeXmlComments(path);
                    }
                }
            });

            services.AddMvc(options =>
            {
                var authPolicy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();

                options.Filters.Add(new AuthorizeFilter(authPolicy));
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHttpsRedirection();
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseAuthentication();

            app.Use(async (context, next) =>
            {
                var principal = new ClaimsPrincipal();
                var identity = new ClaimsIdentity("Mixed.Auth");

                var jwtAuth = await context.AuthenticateAsync(JwtBearerDefaults.AuthenticationScheme);
                if (jwtAuth?.Principal != null)
                {
                    principal.AddIdentities(jwtAuth.Principal.Identities);
                }
                else
                {
                    var winAuth = await context.AuthenticateAsync(IISDefaults.AuthenticationScheme);
                    if (winAuth?.Principal != null)
                    {
                        principal.AddIdentities(winAuth.Principal.Identities);
                    }
                }

                context.User = principal;

                await next();
            });

            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                var section = Configuration.GetSection("Swagger");

                options.SwaggerEndpoint(section["Endpoint"], section["Title"]);

                options.DocExpansion(DocExpansion.None);
            });

            app.UseMvc();
        }
    }
}
