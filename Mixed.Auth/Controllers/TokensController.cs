﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace Mixed.Auth.Controllers
{
    [Route("tokens")]
    public class TokensController : Controller
    {
        [HttpGet, Route("current")]
        public ActionResult Get()
        {
            return GetToken(User.Identity.Name);
        }

        [HttpGet, Route("{user}")]
        public ActionResult Impersonate(string user)
        {
            // TODO: check for superuser, etc
            return GetToken(user);
        }

        private ActionResult GetToken(string user)
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, user),
                new Claim(JwtRegisteredClaimNames.Sub, user),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("74C9C7C7-0954-4330-997A-FA919100C6DE"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                null,
                null,
                claims,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: creds);

            return Ok(new { token = new JwtSecurityTokenHandler().WriteToken(token) });
        }
    }
}
